module.exports = {
  NODE_ENV: '"production"',
  BASE_API: '"https://lisa.unlcn.com/wms-admin"',
  // TODO 生产接口地址
  // BASE_API_INTEGRATION: '"http://10.20.30.138:8780/lisa-integration"',
  BASE_API_INTEGRATION: '"https://lisa.unlcn.com/lisa-integration"',
  BASE_API_LOGIN: '"https://lisa.unlcn.com/uaa"'
}
