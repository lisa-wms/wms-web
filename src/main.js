import Vue from 'vue'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import App from './App.vue'
import router from './router'
import store from './store'
import * as tools from './utils/tools'
import '@/icons' // icon
import '@/permission' // 权限
import '@/utils/dialogDrag' // 弹窗拖拽
import '@/filter/customFilters' // 自定义过滤器
import LODOP from 'static/plugIn/LodopFuncs'
// import './mock'

Vue.use(ElementUI, {
  size: 'small'
})

Vue.config.productionTip = false

Vue.prototype.LODOP = LODOP

// 定义全局的方法类
Vue.prototype.tools = tools

new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})
