import fetch from '@/utils/fetch'

// 异常查询-异常处理
export function exceptionHandling(data) {
  return fetch({
    url: '/OpManagement/exceptionHandling',
    method: 'post',
    data: data
  })
}

// 异常查询-明细分页
export function getExceptionDetail(data) {
  return fetch({
    url: '/OpManagement/getExceptionDetail',
    method: 'post',
    data: data
  })
}

// 异常查询
export function getExceptionRegisterList(data) {
  return fetch({
    url: '/OpManagement/getExceptionList',
    method: 'post',
    data: data
  })
}
// 异常查询-导出
export function exportExcpList(data) {
  return fetch({
    url: '/OpManagement/exportExcpList',
    method: 'post',
    data: data
  })
}

// 异常查询 页- 删除
export function deleteException(data) {
  return fetch({
    url: '/OpManagement/deleteException',
    method: 'post',
    data: data
  })
}

// 异常查询页 -新增
export function saveException(data) {
  return fetch({
    url: '/OpManagement/saveException',
    method: 'post',
    data: data
  })
}

// 重新推送异常
export function pushException(data) {
  return fetch({
    url: '/OpManagement/pushException',
    method: 'post',
    data: data
  })
}
