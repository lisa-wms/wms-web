import fetch from '@/utils/fetch'

// 库位调整记录页-列表查询
export function changeLocList(data) {
  return fetch({
    url: '/inboundPutaway/changeLocList',
    method: 'post',
    data: data
  })
}

// 入库记录导出
export function exportLocList(data) {
  return fetch({
    url: '/inboundPutaway/exportLocList',
    method: 'post',
    data: data
  })
}

