import fetch from '@/utils/fetch'

// 入库通知单查询页-列表查询
export function selectInboundPage(data) {
  return fetch({
    url: '/inboundNotice/selectInboundPage',
    method: 'post',
    data: data
  })
}
export function exportData(data) {
  return fetch({
    url: '/inboundNotice/exportINData',
    method: 'post',
    data: data
  })
}
// 补发的入库数据页-列表查询
export function queryPushInboundOtm(data) {
  return fetch({
    url: '/inboundNotice/queryPushInboundOtm',
    method: 'post',
    data: data
  })
}
//  补发的入库数据至otm
export function pushInboundOtm(data) {
  return fetch({
    url: '/inboundNotice/pushInboundOtm',
    method: 'post',
    data: data
  })
}
// 补发的发运数据页-查询列表
export function queryPushShipmentOtm(data) {
  return fetch({
    url: '/shipment/queryPushShipmentOtm',
    method: 'post',
    data: data
  })
}

// 重推发运数据直 otm/tms
export function pushShipmentOtm(data) {
  return fetch({
    url: '/shipment/pushShipmentOtm',
    method: 'post',
    data: data
  })
}
