import fetch from '@/utils/fetch'

// 入库记录页- 取消入库
export function wmsCancleInbound(data) {
  return fetch({
    url: '/inboundPutaway/wmsCancleInbound',
    method: 'post',
    data: data
  })
}

// 入库钥匙备料页 导出excel
export function exportPutawayKeyData(data) {
  return fetch({
    url: '/inboundPutaway/exportPutawayKeyData',
    method: 'post',
    data: data
  })
}

// 入库钥匙管理页-发放合格证书
export function giveOutCertificat(data) {
  return fetch({
    url: '/inboundPutaway/giveOutCertificat',
    method: 'post',
    data: data
  })
}

// 入库钥匙管理页-领取合格证书
export function receiveOutCertificat(data) {
  return fetch({
    url: '/inboundPutaway/receiveOutCertificat',
    method: 'post',
    data: data
  })
}

// 入库钥匙页 入库记录页 列表查询
export function selectPutawayPage(data) {
  return fetch({
    url: '/inboundPutaway/selectPutawayPage',
    method: 'post',
    data: data
  })
}
// 收取钥匙-获取入库信息
export function getReceiveKeyInfo(data) {
  return fetch({
    url: '/inboundPutaway/getReceiveKeyInfo',
    method: 'post',
    data: data
  })
}
// 收取钥匙-确认收取钥匙
export function confirmReceiveKey(data) {
  return fetch({
    url: '/inboundPutaway/confirmReceiveKey',
    method: 'post',
    data: data
  })
}
// 收取钥匙时打印二维码
export function getKeyPrintInfo(data) {
  return fetch({
    url: '/inboundPutaway/getKeyPrintInfo',
    method: 'post',
    data: data
  })
}
// 入库记录导出
export function exportData(data) {
  return fetch({
    url: '/inboundPutaway/exportINRData',
    method: 'post',
    data: data
  })
}

