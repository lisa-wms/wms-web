import fetch from '@/utils/fetch'

// 入库作业页-列表查询
export function inboundTaskQueryPage(data) {
  return fetch({
    url: '/inboundTask/queryPage',
    method: 'post',
    data: data
  })
}
export function inboundTaskPutAwayList(data) {
  return fetch({
    url: '/inboundTask/putwayList',
    method: 'post',
    data: data
  })
}
export function inboundTaskSeachDetail(data) {
  return fetch({
    url: '/inboundTask/seachDetail',
    method: 'post',
    data: data
  })
}
export function inboundTaskPutWay(data) {
  return fetch({
    url: '/inboundTask/inboundPutWay',
    method: 'post',
    data: data
  })
}
export function queryUsableLocation(data) {
  return fetch({
    url: '/inboundTask/queryArea',
    method: 'post',
    data: data
  })
}

// 生成二维码 /inboundTask/printBarcode
export function printBarcode(data) {
  return fetch({
    url: '/inboundTask/printBarcode',
    method: 'post',
    data: data
  })
}

// 入库作业页-打印二维码面板-收取钥匙确认
export function confirmReceiveKey(data) {
  return fetch({
    url: '/inboundPutaway/confirmReceiveKey',
    method: 'post',
    data: data
  })
}

// 入库作业页-库区面板-库区下拉列表
export function queryValidArea(data) {
  return fetch({
    url: '/storeArea/queryValidArea',
    method: 'post',
    data: data
  })
}
