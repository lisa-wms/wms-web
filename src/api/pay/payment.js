import fetch from '@/utils/fetch'

/**
 *  列表
 * @param data
 */
export function listPayment(data) {
  return fetch({
    url: '/payment/list',
    method: 'post',
    data
  })
}

/**
 * 新增付款单
 * @param data
 */
export function addPayment(data) {
  return fetch({
    url: '/payment/add',
    method: 'post',
    data
  })
}

/**
 * 获取付款单
 * @param data
 */
export function getPayment(id) {
  return fetch({
    url: '/payment/get/' + id,
    method: 'get'
  })
}

/**
 * 更新付款单
 * @param data
 */
export function updatePayment(data) {
  return fetch({
    url: '/payment/update',
    method: 'put',
    data
  })
}

/**
 * 删除付款单
 * @param data
 */
export function deletePayment(data) {
  return fetch({
    url: '/payment/delete',
    method: 'delete',
    data
  })
}

/**
 * 审核付款单
 * @param data
 */
export function auditPayment(data) {
  return fetch({
    url: '/payment/audit',
    method: 'post',
    data
  })
}

