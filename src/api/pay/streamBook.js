import fetch from '@/utils/fetch'

/**
 *  列表
 * @param data
 */
export function listStreamBook(data) {
  return fetch({
    url: '/streamBook/list',
    method: 'post',
    data
  })
}

/**
 * 余额账
 */
export function balanceList(data) {
  return fetch({
    url: '/streamBook/balanceList',
    method: 'post',
    data
  })
}
