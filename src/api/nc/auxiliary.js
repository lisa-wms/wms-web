import fetch from '@/utils/fetch'

// 分页查询
export function queryAuxiliaryPage(data) {
  return fetch({
    url: '/auxiliary/queryPage',
    method: 'post',
    data
  })
}
