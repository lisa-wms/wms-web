import fetch from '@/utils/fetch'

// 分页查询
export function queryPage(data) {
  return fetch({
    url: '/fileContrast/queryPage',
    method: 'post',
    data
  })
}

// 新增
export function create(data) {
  return fetch({
    url: '/fileContrast/create',
    method: 'post',
    data
  })
}

// 修改
export function update(data) {
  return fetch({
    url: '/fileContrast/update',
    method: 'post',
    data
  })
}

// 删除
export function deleteForBatch(data) {
  return fetch({
    url: '/fileContrast/deleteForBatch',
    method: 'post',
    data
  })
}

