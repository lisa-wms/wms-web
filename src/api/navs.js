import fetch from '@/utils/fetch'

export function GetTopNavs() {
  return fetch({
    url: '/user/firstMenu',
    method: 'get'
  })
}

export function GetSideNavs(pid) {
  return fetch({
    url: '/user/subMenu',
    method: 'get',
    params: { pid }
  })
}
