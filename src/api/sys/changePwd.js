import fetch from '@/utils/fetch'

export function examinPassword(password) {
  return fetch({
    url: '/account/examinPassword',
    method: 'get',
    params: { password }
  })
}
export function updatePwd(data) {
  return fetch({
    url: '/user/updatePwd',
    method: 'post',
    data
  })
}
