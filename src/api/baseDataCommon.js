import fetch from '@/utils/fetch'

// 获取供应商信息
export function getPartnerList() {
  return fetch({
    url: '/baseData/getPartnerList',
    method: 'get'
  })
}

// 获取人员信息
export function getStaffList() {
  return fetch({
    url: '/baseData/getStaffList',
    method: 'get'
  })
}

// 获取币种信息
export function getCurrencyTypeList() {
  return fetch({
    url: '/baseData/getCurrencyTypeList',
    method: 'get'
  })
}

// 获取部门信息
export function getDepartmentList() {
  return fetch({
    url: '/baseData/getDepartmentList',
    method: 'get'
  })
}

// 获取应付单类型
export function getShouldPaymentTypeList() {
  return fetch({
    url: '/baseData/getShouldPaymentTypeList',
    method: 'get'
  })
}

// 获取公司
export function getCompanyList() {
  return fetch({
    url: '/baseData/getCompanyList',
    method: 'get'
  })
}

// 获取收支项目
export function getIncomePayItemList() {
  return fetch({
    url: '/baseData/getIncomePayItemList',
    method: 'get'
  })
}

// 获取存货
export function getStockGoodList() {
  return fetch({
    url: '/baseData/getStockGoodList',
    method: 'get'
  })
}

// 获取会计期间
export function getFiscalPeriodList() {
  return fetch({
    url: '/baseData/getFiscalPeriodList',
    method: 'get'
  })
}

// 获取会计期间明细
export function getFiscalPeriodDetailList() {
  return fetch({
    url: '/baseData/getFiscalPeriodDetailList',
    method: 'get'
  })
}

// 获取银行
export function getBankInfoList() {
  return fetch({
    url: '/baseData/getBankInfoList',
    method: 'get'
  })
}

// 获取收付款协议
export function getIncomePayAgreementSelectList() {
  return fetch({
    url: '/baseData/getIncomePayAgreementSelectList',
    method: 'get'
  })
}

// 获取结算方式
export function getSettleTypeList() {
  return fetch({
    url: '/baseData/getSettleTypeList',
    method: 'get'
  })
}

// 获取应付单
export function getShouldPaymentList(data) {
  return fetch({
    url: '/baseData/getShouldPaymentList',
    method: 'post',
    data
  })
}

// 根据业务类型获取应付单类型
export function getShouldPaymentTypeListByBusinessType(data) {
  return fetch({
    url: '/baseData/getShouldPaymentTypeListByBusinessType',
    method: 'post',
    data
  })
}

// 获取存货分类
export function getStockTypeList() {
  return fetch({
    url: '/baseData/getStockTypeList',
    method: 'get'
  })
}

// 获取结算/银行账户
export function getBankAccountList() {
  return fetch({
    url: '/baseData/getBankAccountList',
    method: 'get'
  })
}

// 获取结算/银行账户
export function getBankAccountCodeList() {
  return fetch({
    url: '/baseData/getBankAccountCodeList',
    method: 'get'
  })
}

// 获取现金流量项目
export function getCashFlowLtemList() {
  return fetch({
    url: '/baseData/getCashFlowLtemList',
    method: 'get'
  })
}

// 获取现金流量项目
export function getAccountingSubjectList() {
  return fetch({
    url: '/baseData/getAccountingSubjectList',
    method: 'get'
  })
}

// 获取应付 付款单
export function getPaymentList(data) {
  return fetch({
    url: '/baseData/getPaymentList',
    method: 'post',
    data
  })
}

// 获取地区分类下拉
export function getDistrictList() {
  return fetch({
    url: '/baseData/getDistrictList',
    method: 'get'
  })
}

/**
 * 根据银行id获取银行账号
 */
export function getBankAccountListByBankInfoId(data) {
  return fetch({
    url: '/baseData/getBankAccountListByBankInfoId/' + data,
    method: 'get'
  })
}

// 获取项目辅助核算
export function getProjectItemList() {
  return fetch({
    url: '/baseData/getProjectItemList',
    method: 'get'
  })
}

// 获取应收单类型
export function getShouldReceiptTypeListByBusinessType(data) {
  return fetch({
    url: '/baseData/getShouldReceiptTypeListByBusinessType',
    method: 'post',
    data
  })
}

// 获取应收单类型
export function getShouldReceiptTypeList() {
  return fetch({
    url: '/baseData/getShouldReceiptTypeList',
    method: 'get'
  })
}

// 获取NC客商
export function getNcFileContrastPartnerList() {
  return fetch({
    url: '/baseData/getNcFileContrastPartnerList',
    method: 'get'
  })
}

// 获取应收单列表
export function getShouldReceiptList(data) {
  return fetch({
    url: '/baseData/getShouldReceiptList',
    method: 'post',
    data
  })
}

// 获取内部交易结算单类型
export function getInternalTransactionTypeSelectList() {
  return fetch({
    url: '/baseData/getInternalTransactionTypeSelectList',
    method: 'get'
  })
}

// 获取成本核算的成本要素
export function getCostElementList() {
  return fetch({
    url: '/baseData/getCostElementList',
    method: 'get'
  })
}

// 获取收款单
export function getReceiptList(data) {
  return fetch({
    url: '/baseData/getReceiptList',
    method: 'post',
    data
  })
}

// 获取客户信息
export function getCustomerList() {
  return fetch({
    url: '/baseData/getCustomerList',
    method: 'get'
  })
}

// 获取费用分组信息
export function getCostAllotList() {
  return fetch({
    url: '/baseData/getCostAllotList',
    method: 'get'
  })
}

// 获取收入数据
export function getCaIncomeDataList(data) {
  return fetch({
    url: '/baseData/getCaIncomeDataList',
    method: 'post',
    data
  })
}

// 获取所有的客商供应商
export function getAllBusinessPartnerList() {
  return fetch({
    url: '/baseData/getAllBusinessPartnerList',
    method: 'get'
  })
}

// 根据headId获取应付单详情
export function getShouldPaymentDetailByHeadId(data) {
  return fetch({
    url: '/baseData/getShouldPaymentDetailByHeadId/' + data,
    method: 'get'
  })
}
