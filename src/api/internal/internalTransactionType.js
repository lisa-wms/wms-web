import fetch from '@/utils/fetch'

// 分页
export function queryPage(data) {
  return fetch({
    url: '/transactionType/queryPage',
    method: 'post',
    data
  })
}

// 新增
export function addTransactionType(data) {
  return fetch({
    url: '/transactionType/addTransactionType',
    method: 'post',
    data
  })
}

// 编辑
export function updateTransactionType(data) {
  return fetch({
    url: '/transactionType/updateTransactionType',
    method: 'post',
    data
  })
}

// 删除
export function deleteForBatch(data) {
  return fetch({
    url: '/transactionType/deleteForBatch',
    method: 'post',
    data
  })
}
