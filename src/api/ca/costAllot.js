import fetch from '@/utils/fetch'

// 分页查询
export function queryPage(data) {
  return fetch({
    url: '/costAllot/queryPage',
    method: 'post',
    data
  })
}

// 新增
export function addCostAllot(data) {
  return fetch({
    url: '/costAllot/addCostAllot',
    method: 'post',
    data
  })
}

// 编辑
export function updateCostAllot(data) {
  return fetch({
    url: '/costAllot/updateCostAllot',
    method: 'post',
    data
  })
}

// 删除
export function deleteForBatch(data) {
  return fetch({
    url: '/costAllot/deleteForBatch',
    method: 'post',
    data
  })
}

// 获取树形数据
export function getCostAllotTreeData() {
  return fetch({
    url: '/costAllot/getCostAllotTreeData',
    method: 'get'
  })
}
