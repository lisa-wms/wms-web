import fetch from '@/utils/fetch'

// 分页查询
export function queryPage(data) {
  return fetch({
    url: '/caGrniCost/queryPage',
    method: 'post',
    data
  })
}

// 新增
export function addCaGrniCost(data) {
  return fetch({
    url: '/caGrniCost/addCaGrniCost',
    method: 'post',
    data
  })
}

// 编辑
export function updateCaGrniCost(data) {
  return fetch({
    url: '/caGrniCost/updateCaGrniCost',
    method: 'post',
    data
  })
}

// 删除
export function deleteForBatch(data) {
  return fetch({
    url: '/caGrniCost/deleteForBatch',
    method: 'post',
    data
  })
}

// 获取数据
export function getCaGrniCostById(id) {
  return fetch({
    url: '/caGrniCost/getCaGrniCostById/' + id,
    method: 'get'
  })
}
