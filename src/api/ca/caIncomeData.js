import fetch from '@/utils/fetch'

// 分页查询
export function queryPage(data) {
  return fetch({
    url: '/caIncomeData/queryPage',
    method: 'post',
    data
  })
}

// 新增
export function addCaIncomeData(data) {
  return fetch({
    url: '/caIncomeData/addCaIncomeData',
    method: 'post',
    data
  })
}

// 编辑
export function updateCaIncomeData(data) {
  return fetch({
    url: '/caIncomeData/updateCaIncomeData',
    method: 'post',
    data
  })
}

// 删除
export function deleteForBatch(data) {
  return fetch({
    url: '/caIncomeData/deleteForBatch',
    method: 'post',
    data
  })
}

// 获取数据
export function getCaIncomeDataById(id) {
  return fetch({
    url: '/caIncomeData/getCaIncomeDataById/' + id,
    method: 'get'
  })
}
