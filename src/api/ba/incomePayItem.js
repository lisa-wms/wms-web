import fetch from '@/utils/fetch'

/**
 *  列表
 * @param data
 */
export function listIncomePayItem(data) {
  return fetch({
    url: '/incomePayItem/list',
    method: 'post',
    data
  })
}

/**
 * 获取收支项目树
 * @param data
 */
export function getIncomePayItemTree() {
  return fetch({
    url: '/incomePayItem/getIncomePayItemTree',
    method: 'get'
  })
}

/**
 * 新增收支项目
 * @param data
 */
export function addIncomePayItem(data) {
  return fetch({
    url: '/incomePayItem/add',
    method: 'post',
    data
  })
}

/**
 * 获取收支项目
 * @param data
 */
export function getIncomePayItem(id) {
  return fetch({
    url: '/incomePayItem/get/' + id,
    method: 'get'
  })
}

/**
 * 更新收支项目
 * @param data
 */
export function updateIncomePayItem(data) {
  return fetch({
    url: '/incomePayItem/update',
    method: 'put',
    data
  })
}

/**
 * 删除收支项目
 * @param data
 */
export function deleteIncomePayItem(data) {
  return fetch({
    url: '/incomePayItem/delete',
    method: 'delete',
    data
  })
}
