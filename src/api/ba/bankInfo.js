import fetch from '@/utils/fetch'

export function queryPage(data) {
  return fetch({
    url: '/bankInfo/queryListForPage',
    method: 'post',
    data
  })
}

export function getBankTypeList() {
  return fetch({
    url: '/bankType/getBankTypeList',
    method: 'get'
  })
}

export function create(data) {
  return fetch({
    url: '/bankInfo/addBankInfo',
    method: 'post',
    data
  })
}

export function update(data) {
  return fetch({
    url: '/bankInfo/updateBankInfo',
    method: 'post',
    data
  })
}

export function deleted(id) {
  return fetch({
    url: '/bankInfo/deleteBankInfo',
    method: 'get',
    params: { id }
  })
}

export function deletedForBatch(data) {
  return fetch({
    url: '/bankInfo/deleteForBatch',
    method: 'post',
    data
  })
}

export function getBankInfoSelectList(data) {
  return fetch({
    url: '/bankInfo/getBankInfoSelectList',
    method: 'post',
    data
  })
}
