import fetch from '@/utils/fetch'

// 列表
export function queryPage(data) {
  return fetch({
    url: '/currencyType/queryPage',
    method: 'post',
    data
  })
}

// 新增
export function add(data) {
  return fetch({
    url: '/currencyType/add',
    method: 'post',
    data
  })
}

// 删除
export function deleted(data) {
  return fetch({
    url: '/currencyType/deleteCurrencyType',
    method: 'post',
    data
  })
}

// 根据id查找币种信息
export function findById(id) {
  return fetch({
    url: '/currencyType/findById?id=' + id,
    method: 'get'
  })
}

// 更新币种信息
export function modify(data) {
  return fetch({
    url: '/currencyType/modify',
    method: 'post',
    data
  })
}

