import fetch from '@/utils/fetch'

export function queryPage(data) {
  return fetch({
    url: '/businessPartner/queryPage',
    method: 'post',
    data
  })
}

export function add(data) {
  return fetch({
    url: '/businessPartner/add',
    method: 'post',
    data
  })
}

export function update(data) {
  return fetch({
    url: '/businessPartner/modify',
    method: 'post',
    data
  })
}

export function deleted(data) {
  return fetch({
    url: '/businessPartner/deleteBusinessPartner',
    method: 'post',
    data
  })
}

