import fetch from '@/utils/fetch'

/**
 *  公司列表
 * @param data
 */
export function listCompany(data) {
  return fetch({
    url: '/company/list',
    method: 'post',
    data
  })
}

/**
 * 获取公司树
 * @param data
 */
export function getCompanyTree() {
  return fetch({
    url: '/company/getCompanyTree',
    method: 'get'
  })
}

/**
 * 新增公司
 * @param data
 */
export function addCompany(data) {
  return fetch({
    url: '/company/add',
    method: 'post',
    data
  })
}

/**
 * 获取公司
 * @param data
 */
export function getCompany(id) {
  return fetch({
    url: '/company/get/' + id,
    method: 'get'
  })
}

/**
 * 更新公司
 * @param data
 */
export function updateCompany(data) {
  return fetch({
    url: '/company/update',
    method: 'put',
    data
  })
}

/**
 * 删除公司
 * @param data
 */
export function deleteCompany(data) {
  return fetch({
    url: '/company/delete',
    method: 'delete',
    data
  })
}

/**
 * 查询所有公司
 * @param data
 */
export function getAllCompany() {
  return fetch({
    url: '/company/getAll',
    method: 'get'
  })
}

