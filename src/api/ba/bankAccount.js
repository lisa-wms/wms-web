import fetch from '@/utils/fetch'
export function queryPage(data) {
  return fetch({
    url: '/bankAccount/queryListForPage',
    method: 'post',
    data
  })
}

export function create(data) {
  return fetch({
    url: '/bankAccount/addBankAccount',
    method: 'post',
    data
  })
}

export function deleted(id) {
  return fetch({
    url: '/bankAccount/deleteBankAccount',
    method: 'get',
    params: { id }
  })
}

export function deletedForBatch(data) {
  return fetch({
    url: '/bankAccount/deleteForBatch',
    method: 'post',
    data
  })
}

export function update(data) {
  return fetch({
    url: '/bankAccount/updateBankAccount',
    method: 'post',
    data
  })
}
