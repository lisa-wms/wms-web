import fetch from '@/utils/fetch'

export function queryPage(data) {
  return fetch({
    url: '/settleType/queryPage',
    method: 'post',
    data
  })
}

export function create(data) {
  return fetch({
    url: '/settleType/create',
    method: 'post',
    data
  })
}

export function update(data) {
  return fetch({
    url: '/settleType/update',
    method: 'post',
    data
  })
}

export function deleted(data) {
  return fetch({
    url: '/settleType/delete',
    method: 'post',
    data
  })
}

