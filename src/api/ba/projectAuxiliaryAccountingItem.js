import fetch from '@/utils/fetch'

/**
 *  列表
 * @param data
 */
export function listProjectAuxiliaryAccountingItem(data) {
  return fetch({
    url: '/projectAuxiliaryAccountingItem/list',
    method: 'post',
    data
  })
}

/**
 * 新增项目辅助核算项
 * @param data
 */
export function addProjectAuxiliaryAccountingItem(data) {
  return fetch({
    url: '/projectAuxiliaryAccountingItem/add',
    method: 'post',
    data
  })
}

/**
 * 获取项目辅助核算项
 * @param data
 */
export function getProjectAuxiliaryAccountingItem(id) {
  return fetch({
    url: '/projectAuxiliaryAccountingItem/get/' + id,
    method: 'get'
  })
}

/**
 * 更新项目辅助核算项
 * @param data
 */
export function updateProjectAuxiliaryAccountingItem(data) {
  return fetch({
    url: '/projectAuxiliaryAccountingItem/update',
    method: 'put',
    data
  })
}

/**
 * 删除项目辅助核算项
 * @param data
 */
export function deleteProjectAuxiliaryAccountingItem(data) {
  return fetch({
    url: '/projectAuxiliaryAccountingItem/delete',
    method: 'delete',
    data
  })
}
