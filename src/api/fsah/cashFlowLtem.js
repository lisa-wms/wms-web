import fetch from '@/utils/fetch'

// 新增
export function add(data) {
  return fetch({
    url: '/cashFlowLtem/add',
    method: 'post',
    data
  })
}

// 编辑
export function update(data) {
  return fetch({
    url: '/cashFlowLtem/update',
    method: 'post',
    data
  })
}

// 加载树形数据
export function getCashFlowTreeListByParentId(data) {
  return fetch({
    url: '/cashFlowLtem/getCashFlowTreeListByParentId',
    method: 'post',
    data
  })
}

// 加载树形数据
export function getCashFlowTreeData() {
  return fetch({
    url: '/cashFlowLtem/getCashFlowTreeData',
    method: 'get'
  })
}

// 加载树形数据
export function deleteById(id) {
  return fetch({
    url: '/cashFlowLtem/deleteById/' + id,
    method: 'get'
  })
}

// 加载树形数据
export function getCashFlowById(id) {
  return fetch({
    url: '/cashFlowLtem/getCashFlowById/' + id,
    method: 'get'
  })
}
