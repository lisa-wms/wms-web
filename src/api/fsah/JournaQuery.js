import fetch from '@/utils/fetch'

// 加载树形数据
export function queryPage(data) {
  return fetch({
    url: '/journaQuery/queryPage',
    method: 'post',
    data
  })
}
