import fetch from '@/utils/fetch'

export function listUser(data) {
  return fetch({
    url: '/user/list',
    method: 'post',
    data
  })
}

export function detailUser(uid) {
  return fetch({
    url: '/role/subMenu',
    method: 'get',
    params: { uid }
  })
}
