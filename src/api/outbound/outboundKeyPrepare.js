import fetch from '@/utils/fetch'

// 出库钥匙备料页-批量打印
export function batchPrintPrepare(data) {
  return fetch({
    url: '/keyPrepare/batchPrintPrepare',
    method: 'post',
    data: data
  })
}
// 出库钥匙备料页-列表查询
export function queryKeyPreparePage(data) {
  return fetch({
    url: '/keyPrepare/queryKeyPreparePage',
    method: 'post',
    data: data
  })
}
export function getKeyPrepareDetail(data) {
  return fetch({
    url: '/keyPrepare/getKeyPrepareDetail',
    method: 'post',
    data: data
  })
}
export function updateKeyPrepareStatus(data) {
  return fetch({
    url: '/keyPrepare/updateKeyPrepareStatus',
    method: 'post',
    data: data
  })
}

// 出库钥匙备料页-确认备料
export function confirmPrepare(data) {
  return fetch({
    url: '/keyPrepare/confirmPrepare',
    method: 'post',
    data: data
  })
}
