import fetch from '@/utils/fetch'

/**
 *  列表
 * @param data
 */
export function listReceipt(data) {
  return fetch({
    url: '/receipt/list',
    method: 'post',
    data
  })
}

/**
 * 新增付款单
 * @param data
 */
export function addReceipt(data) {
  return fetch({
    url: '/receipt/add',
    method: 'post',
    data
  })
}

/**
 * 获取付款单
 * @param data
 */
export function getReceipt(id) {
  return fetch({
    url: '/receipt/get/' + id,
    method: 'get'
  })
}

/**
 * 更新付款单
 * @param data
 */
export function updateReceipt(data) {
  return fetch({
    url: '/receipt/update',
    method: 'put',
    data
  })
}

/**
 * 删除付款单
 * @param data
 */
export function deleteReceipt(data) {
  return fetch({
    url: '/receipt/delete',
    method: 'delete',
    data
  })
}

/**
 * 审核付款单
 * @param data
 */
export function auditReceipt(data) {
  return fetch({
    url: '/receipt/audit',
    method: 'post',
    data
  })
}
/**
 * 弃审
 * @param data
 */
export function abandonTrial(data) {
  return fetch({
    url: '/receipt/abandonTrial',
    method: 'post',
    data
  })
}

