import fetch from '@/utils/fetch'

/**
 *  列表
 * @param data
 */
export function listIncWriteOff(data) {
  return fetch({
    url: '/incWriteOff/list',
    method: 'post',
    data
  })
}

/**
 * 新增核销单类型
 * @param data
 */
export function addIncWriteOff(data) {
  return fetch({
    url: '/incWriteOff/add',
    method: 'post',
    data
  })
}

/**
 * 获取核销单类型
 * @param data
 */
export function getIncWriteOff(id) {
  return fetch({
    url: '/incWriteOff/get/' + id,
    method: 'get'
  })
}

/**
 * 更新核销单类型
 * @param data
 */
export function updateIncWriteOff(data) {
  return fetch({
    url: '/incWriteOff/update',
    method: 'put',
    data
  })
}

/**
 * 删除核销单类型
 * @param data
 */
export function deleteIncWriteOff(data) {
  return fetch({
    url: '/incWriteOff/delete',
    method: 'delete',
    data
  })
}

/**
 * 审核付款单
 * @param data
 */
export function auditIncWriteOff(data) {
  return fetch({
    url: '/incWriteOff/audit',
    method: 'post',
    data
  })
}
