import fetch from '@/utils/fetch'
export function getIncomeOrderList(data) {
  return fetch({
    url: '/incomeOrder/getIncomeOrderList',
    method: 'post',
    data
  })
}
