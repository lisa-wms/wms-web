import fetch from '@/utils/fetch'

/**
 *  列表
 * @param data
 */
export function listShouldReceiptType(data) {
  return fetch({
    url: '/shouldReceiptType/list',
    method: 'post',
    data
  })
}

/**
 * 新增应收单类型
 * @param data
 */
export function addShouldReceiptType(data) {
  return fetch({
    url: '/shouldReceiptType/add',
    method: 'post',
    data
  })
}

/**
 * 获取应收单类型
 * @param data
 */
export function getShouldReceiptType(id) {
  return fetch({
    url: '/shouldReceiptType/get/' + id,
    method: 'get'
  })
}

/**
 * 更新应收单类型
 * @param data
 */
export function updateShouldReceiptType(data) {
  return fetch({
    url: '/shouldReceiptType/update',
    method: 'put',
    data
  })
}

/**
 * 删除应收单类型
 * @param data
 */
export function deleteShouldReceiptType(data) {
  return fetch({
    url: '/shouldReceiptType/delete',
    method: 'delete',
    data
  })
}

/**
 * 根据业务类型获取付款单类型
 * @param data
 */
export function getByBusinessType(data) {
  return fetch({
    url: '/shouldReceiptType/getByBusinessType',
    method: 'post',
    data
  })
}

