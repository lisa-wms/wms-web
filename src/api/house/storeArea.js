import fetch from '@/utils/fetch'

// 批量禁用库区/库位
export function batchUpdateStatus(data) {
  return fetch({
    url: '/storeArea/batchUpdateStatus',
    method: 'post',
    data: data
  })
}

// 库区查询
export function queryByPage(data) {
  return fetch({
    url: '/storeArea/queryByPage',
    method: 'post',
    data: data
  })
}

// 新增库区
export function addStoreArea(data) {
  return fetch({
    url: '/storeArea/addStoreArea',
    method: 'post',
    data: data
  })
}

// 编辑库区
export function updateStoreArea(data) {
  return fetch({
    url: '/storeArea/updateStoreArea',
    method: 'post',
    data: data
  })
}
// 批量 修改 库位类型
export function batchUpdateLocationType(data) {
  return fetch({
    url: '/storeArea/batchUpdateLocationType',
    method: 'post',
    data: data
  })
}
